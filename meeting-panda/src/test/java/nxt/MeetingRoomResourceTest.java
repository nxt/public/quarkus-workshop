package nxt;

import io.quarkus.test.junit.QuarkusTest;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.transaction.Transactional;
import javax.ws.rs.core.MediaType;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

@QuarkusTest
public class MeetingRoomResourceTest {

    @BeforeEach
    @Transactional
    public void cleanupDb() {
        Reservation.deleteAll();
        MeetingRoom.deleteAll();
    }

    @Test
    public void testCreate() {
        MeetingRoom meetingRoom = new MeetingRoom();
        meetingRoom.name = "Room 1";
        meetingRoom.size = 2;

        String location = given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(meetingRoom)
                .when().post("/rooms")
                .getHeader("Location");
        long id = Long.parseLong(location.substring(location.lastIndexOf("/") + 1));

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/rooms")
                .then()
                .body("[0].name", equalTo("Room 1"))
                .statusCode(HttpStatus.SC_OK);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/rooms/" + id)
                .then()
                .body("name", equalTo("Room 1"))
                .statusCode(HttpStatus.SC_OK);

        meetingRoom.name = "Room new 1";

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(meetingRoom)
                .when().put("/rooms/" + id)
                .then()
                .statusCode(204);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/rooms/" + id)
                .then()
                .body("name", equalTo("Room new 1"))
                .statusCode(HttpStatus.SC_OK);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().delete("/rooms/" + id)
                .then()
                .statusCode(HttpStatus.SC_NO_CONTENT);

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .when().get("/rooms/" + id)
                .then()
                .statusCode(HttpStatus.SC_NOT_FOUND);
    }

}