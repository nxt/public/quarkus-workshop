package nxt;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.time.Month;

@QuarkusTest
public class ReservationServiceTest {

    @Inject
    ReservationService reservationService;

    @BeforeEach
    @Transactional
    public void cleanupDb() {
        Reservation.deleteAll();
        MeetingRoom.deleteAll();
    }

    @Test
    @Transactional
    public void test() {
        LocalDateTime from = LocalDateTime.of(2019, Month.DECEMBER, 24, 10, 00);
        LocalDateTime to = from.plusHours(2);

        MeetingRoom room = new MeetingRoom();
        room.name = "Test";
        room.persist();

        Assertions.assertTrue(reservationService.isRoomAvailable(room.id, from, to));

        Reservation reservation = new Reservation();
        reservation.meetingRoom = room;
        reservation.from = from;
        reservation.to = to;
        reservation.persist();

        Assertions.assertFalse(reservationService.isRoomAvailable(room.id, from, to));
    }
}
