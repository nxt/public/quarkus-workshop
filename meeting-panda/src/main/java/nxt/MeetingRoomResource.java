package nxt;

import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/rooms")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MeetingRoomResource {

    @Context
    UriInfo uri;

    @GET
    public List<MeetingRoom> list() {
        return MeetingRoom.listAll();
    }

    @GET
    @Path("{meetingRoomId}")
    public Response get(@PathParam("meetingRoomId") Long meetingRoomId) {
        MeetingRoom room = MeetingRoom.findById(meetingRoomId);
        if (room == null) {
            return Response.status(404).build();
        }
        return Response.ok(room).build();
    }

    @POST
    @Transactional
    public Response create(MeetingRoom meetingRoom) {
        meetingRoom.persist();
        return Response.created(uri.getAbsolutePathBuilder().path(String.valueOf(meetingRoom.id)).build()).build();
    }

    @PUT
    @Transactional
    @Path("{meetingRoomId}")
    public void update(@PathParam("meetingRoomId") Long meetingRoomId, MeetingRoom meetingRoom) {
        MeetingRoom room = MeetingRoom.findById(meetingRoomId);
        room.name = meetingRoom.name;
        room.size = meetingRoom.size;
        room.persist();
    }

    @DELETE
    @Transactional
    @Path("{meetingRoomId}")
    public void delete(@PathParam("meetingRoomId") Long meetingRoomId) {
        MeetingRoom room = MeetingRoom.findById(meetingRoomId);
        if (room != null) {
            room.delete();
        }
    }

    @GET
    @Path("kill")
    public void kill() {
        System.exit(1);
    }

}