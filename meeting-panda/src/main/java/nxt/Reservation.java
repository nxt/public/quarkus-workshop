package nxt;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Reservation extends PanacheEntity {
    @Column(name = "reservationFrom")
    public LocalDateTime from;

    @Column(name = "reservationTo")
    public LocalDateTime to;

    public String name;

    @ManyToOne()
    public MeetingRoom meetingRoom;
}
