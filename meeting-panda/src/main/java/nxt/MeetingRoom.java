package nxt;

import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class MeetingRoom extends PanacheEntity {
    public String name;
    public Integer size;
}
