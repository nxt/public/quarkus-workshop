package nxt;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.List;

@Path("/reservations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ReservationResource {

    @Context
    UriInfo uri;

    @Inject
    ReservationService reservationService;

    @GET
    public List<Reservation> list() {
        return Reservation.listAll();
    }

    @POST
    @Transactional
    public Response create(Reservation reservation) {
        if (reservationService.isRoomAvailable(reservation.meetingRoom.id, reservation.from, reservation.to)) {
            reservation.persist();
            return Response.created(uri.getAbsolutePathBuilder().path(String.valueOf(reservation.id)).build()).build();
        } else {
            return Response.status(400).build();
        }
    }

}
