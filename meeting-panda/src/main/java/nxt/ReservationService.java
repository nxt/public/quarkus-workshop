package nxt;

import io.quarkus.panache.common.Parameters;

import javax.enterprise.context.Dependent;
import java.time.LocalDateTime;

@Dependent
public class ReservationService {
    public boolean isRoomAvailable(Long id, LocalDateTime from, LocalDateTime to) {
        return Reservation.find("reservationFrom < :to and reservationTo > :from and meetingRoom.id = :meetingRoomId",
                Parameters.with("from", from).and("to", to).and("meetingRoomId", id)).count() == 0;
    }
}
