package nxt;

import io.agroal.api.AgroalDataSource;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;
import org.eclipse.microprofile.health.Readiness;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.sql.SQLException;

@Readiness
@ApplicationScoped
public class DatabaseConnectionHealthCheck implements HealthCheck {

    @Inject
    AgroalDataSource agroalDataSource;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder check = HealthCheckResponse.named("Database connection health check");
        try {
            return check.state(agroalDataSource.getConnection().isValid(1)).build();
        } catch (SQLException e) {
            return check.down().build();
        }
    }
}