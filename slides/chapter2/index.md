# Getting Started with Quarkus

##  Create Project

```
mvn io.quarkus:quarkus-maven-plugin:0.21.2:create \
    -DprojectGroupId=nxt \
    -DprojectArtifactId=meeting-panda \
    -DclassName="nxt.MeetingRoomResource" \
    -Dpath="/rooms" \
    -Dextensions="resteasy-jsonb"
```

## Java

### Run
`./mvnw compile quarkus:dev`

### Test
`./mvnw test`

### Build
`./mvnw package`

## Native

### Use GraalVM
`export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)`

### Build
`./mvnw package -Pnative`

### Test
`./mvnw verify -Pnative`

### Run
`target/meeting-panda-1.0-SNAPSHOT-runner`

## Docker

### Build
`./mvnw package -Pnative -Dnative-image.docker-build=true`

`docker build -f src/main/docker/Dockerfile.native -t nxt/meeting-panda .`

### RUN
`docker run -i --rm -p 8080:8080 nxt/meeting-panda`

## Workshop Project

1. Create a new quarkus application
2. Start the application
3. Open http://localhost:8080/rooms in your browser and test the application
4. Execute all tests
5. Build native image
6. Build docker image
7. Run docker image