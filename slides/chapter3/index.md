# JAX-RS Extension

## Maven

```xml
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-resteasy</artifactId>
</dependency>
<dependency>
  <groupId>io.quarkus</groupId>
  <artifactId>quarkus-resteasy-jsonb</artifactId>
</dependency>
```

##  Java Resource Class

\scriptsize

```java
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/rooms")
public class MeetingRoomResource {
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{roomId}")
    public Room getRoom(@PathParam("roomId") Long roomId, 
                      @QueryParam("full") boolean full) {
        return ...
    }
}
```

\normalsize

##  Java Resource Test with RestAssured

```java
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;
import static io.restassured.RestAssured.given;

@QuarkusTest
public class MeetingRoomResourceTest {
    @Test
    public void testGetRoom() {
        given()
                .param("full", true)
                .when().get("/rooms/1")
                .then()
                .statusCode(200);
    }
}
```

##  Java Resource Test for Native Tests

```java
import io.quarkus.test.junit.SubstrateTest;

@SubstrateTest
public class NativeMeetingRoomResourceIT 
    extends MeetingRoomResourceTest {

    // Execute the same tests but in native mode.
}
```

## Workshop Project

1. Create a new resource to manage rooms.
2. Create dummy methods for each CRUD operation. The method body can contain a simple `System.out`.
