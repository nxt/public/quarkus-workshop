---
title: Quarkus Workshop
author:
  - Michael Gerber
institute:
  - nxt Engineering GmbH
date: 2019-09-10

# html slides
lang: en-CH
revealjs-url: https://revealjs.com

# latex/pdf slides
# Possible sizes are 8pt, 9pt, 10pt, 11pt, 12pt, 14pt, 17pt, 20pt.
fontsize: 10pt 
aspectratio: 169
header-includes:
  - \definecolor{mDarkTeal}{RGB}{0, 61, 113}
  - \setmainfont{Roboto}
---

!include chapter1/index.md

!include chapter2/index.md

!include chapter3/index.md

!include chapter4/index.md

!include chapter5/index.md

!include chapter6/index.md

!include chapter7/index.md

## {.standout}

Michael Gerber

michael.gerber@nxt.engineering

https://nxt.engineering