# Health Checks

## Maven

`mvn quarkus:add-extension -Dextensions="health"`

```xml
<dependency>
  <groupId>io.quarkus</groupId>
  <artifactId>quarkus-smallrye-health</artifactId>
</dependency>
```

## Health Checks

- `/health/live` - The application is up and running. 

- `/health/ready` - The application is ready to serve requests. 

- `/health` - Accumulating all health check procedures in the application.

## Custom Health Check

\scriptsize

```java
@Readiness
@ApplicationScoped
public class DatabaseConnectionHealthCheck implements HealthCheck {

    @Inject
    AgroalDataSource ds;

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder check = HealthCheckResponse
            .named("Database connection health check");
        try {
            boolean valid = ds.getConnection().isValid(1);
            return check.state(valid).build();
        } catch (SQLException e) {
            return check.down().build();
        }
    }
}
```

\normalsize

## Custom Health Check Result

```json
{
  "status": "UP",
  "checks": [
    {
      "name": "Database connection health check",
      "status": "UP"
    }
  ]
}
```


## Workshop Project

1. Add some custom health checks to your application.