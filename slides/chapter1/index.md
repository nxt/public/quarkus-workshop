# Overview

## Quarkus

A Kubernetes Native Java stack tailored for GraalVM & OpenJDK HotSpot, crafted from the best of breed Java libraries and standards.

Developer: RedHat

License: Apache License version 2.0

## GraalVM

GraalVM is a universal virtual machine for running applications written in JavaScript, Python, Ruby, R, JVM-based languages like Java, Scala, Clojure, Kotlin, and LLVM-based languages such as C and C++.

Developer: Oracle

License: 

- Comunity Edition (GPLv2)
- Enterprise Edition

## Performance benefits of native images

### Memory Footprint (~5x lower)

Melidon: 31 / 106 MB

Micronaut: 41 / 180 MB

Quarkus: 17 / 180 MB

### Startup Time (~50x faster)

Melidon: 35 / 988 ms

Micronaut: 37 / 2101 ms

Quarkus: 16 / 940 ms

## Requirements

- IDE (IntelliJ, VSCode)
- JDK 1.8 or 11 (12 does not work)
- GraalVM 19.1.1
- Maven 3.5+

## Workshop Project

Application to manage meeting rooms.