# Run on Kubernetes

## Getting started

`./mvnw package -Pnative -Dnative-image.docker-build=true`

`docker build -f src/main/docker/Dockerfile.native -t nxt/meeting-panda .`

`kubectl create -f kubernetes.yml`

`kubectl get pods -w`

## Workshop Project

1. Run the application on your local kubernetes.