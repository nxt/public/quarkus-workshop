# Hibernate Panache Extension

## Maven

`mvn quarkus:add-extension -Dextensions="quarkus-hibernate-orm-panache, quarkus-jdbc-postgresql"`



```xml
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-hibernate-orm-panache</artifactId>
</dependency>
<dependency>
    <groupId>io.quarkus</groupId>
    <artifactId>quarkus-jdbc-postgresql</artifactId>
</dependency>
```



##  application.properties



```properties
# configure your datasource
quarkus.datasource.url= jdbc:postgresql://localhost:5432/postgres
quarkus.datasource.driver= org.postgresql.Driver
quarkus.datasource.username= postgres
quarkus.datasource.password= password

# drop and create the database at startup 
# (use 'update' to only update the schema)
quarkus.hibernate-orm.database.generation = drop-and-create

```



##  Entity



```java
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;

@Entity
public class MeetingRoom extends PanacheEntity {
    public String name;
    public Integer size;
}
```



##  Operations

`MeetingRoom.listAll();`

`MeetingRoom.list("name", "Room 1");`

`MeetingRoom.findById(1l);`

`meetinRoom.persist();`

`meetinRoom.persistAndFlush();`

`meetinRoom.delete();`

**Persist and delete actions require a transaction. This can be done by annotating the method with `@Transactional`**

## Workshop Project

1. Start a postgres database on your local computer with docker.
2. Add persistency to the room management.


##  Get (Solution)



```java
@GET
public List<MeetingRoom> list() {
    return  MeetingRoom.findById(meetingRoomId);
}

@GET
@Path("{meetingRoomId}")
public Response get(@PathParam("meetingRoomId") Long meetingRoomId) {
    MeetingRoom room = MeetingRoom.findById(meetingRoomId);
    if (room == null) {
        return Response.status(404).build();
    }
    return Response.ok(room).build();
}
```



##  Create / Post (Solution)



```java
@Context
UriInfo uri;

@POST
@Transactional
public Response create(MeetingRoom meetingRoom) {
    meetingRoom.persist();
    return Response.created(uri.getAbsolutePathBuilder()
            .path(String.valueOf(meetingRoom.id)).build()).build();
}

```



##  Update / Put (Solution)



```java
@PUT
@Transactional
@Path("{meetingRoomId}")
public void update(@PathParam("meetingRoomId") Long meetingRoomId, 
                   MeetingRoom meetingRoom) {
    MeetingRoom room = MeetingRoom.findById(meetingRoomId);
    room.name = meetingRoom.name;
    room.size = meetingRoom.size;
    room.persist();
}
```



##  Delete (Solution)



```java
@DELETE
@Transactional
@Path("{meetingRoomId}")
public void delete(@PathParam("meetingRoomId") Long meetingRoomId) {
    MeetingRoom room = MeetingRoom.findById(meetingRoomId);
    if (room != null) {
        room.delete();
    }
}
```

