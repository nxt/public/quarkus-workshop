# Context and Dependency Injection

## Injections

```java
@ApplicationScoped
public class MyBean {

    @Inject
    OtherBeanService otherService; 

    private BeanService service;

    MyBean(BeanService service) { 
      this.service = service;
    }
}
```

## Scopes

| Scope      | Life-Cycle      | 
|------------|------------------------|
| @Dependent | Bound on life-cycle of "parent" bean  | 
| @Singleton | One instance per application  | 
| @ApplicationScoped |  One instance per application  | 
| @RequestScoped | One instance per request |   
| @SessionScoped | One instance per session |  

## Interceptors

- @AroundInvoke
- @PostConstruct
- @PreDestroy
- @AroundConstruct

## Workshop Project

1. Create a bean which contains the logic to check if a room is available.
2. Create a new resource to manage reservations.
3. Create a method to make a new reservation for a certain room at a certain time. Use the previously created bean in order to check if the room is available.
4. Write some tests.